# Recording from Multiple Webcams
Geoff Nagy  
geoff.nagy@gmail.com

## How to Use

This is a handy script I wrote one afternoon to handle simultaneous recording of multiple webcams that doesn't involve screwing around with ROS.

It's a Python script, so invoke it like this:

	python record.py <output_dir> <cam_1> ... <cam_N>

An example would be:

	python record.py output /dev/video0 /dev/video1 /dev/video2

This will create a folder called "output" (with the current date and time appended), where the video files will go. The video streams themselves will be named "cam0", "cam1", etc., depending on how many devices you've specified. There are no explicitly enforced limits on the number of webcams you can record from, except for whatever maximum number your computer can support.

This script relies only on the "streamer" program. You can get it from apt-get if you don't have it:

	sudo apt-get install streamer

Before starting to record, the script will show you what it is going to do, and ask you to confirm. Once you confirm, the recording will start. (Or, you can abort.) The streamer processes that are invoked will output some data on stdout for debugging/info purposes. I'd recommend leaving that output enabled.

Note that if a video device is already open elsewhere or does not support the video settings specified in the Python script, the video recording will fail and the output from the invoked streamer process will indicate this. After you start recording, it's best to make sure all video files you expect to be in the specified output folder are actually there and being written to.

If you run into problems, it's probably a good idea to run:

	ls -ltrh /dev/video*

or

	v4l2-ctl --list-devices

to see which camera devices you have available, and also

	v4l2-ctl --list-formats-ext

to see what video capabilities are supported by your cameras.

Video recording settings (e.g., resolution, compression, frame rate, etc.) are contained as global variables inside the Python script. They should be widely supported, but you can change them if you wish. However, I've done my best to choose video settings that look good, don't take up too much file space, and are widely supported. Consult the first few lines of record.py to see what those are.

After recording starts, terminate it any time with Ctrl-C.

**NOTE**: The maximum video recording time has been capped at 99 days. Unless you're doing extremely long-term autonomy, this should be sufficient for your needs. You'd run out of hard drive space anyways.

**NOTE**: I've used this script successfully a number of times to record from 3 cameras simultaneously for at most a few minutes long (for my PhD thesis experiments involving drones), using the default settings in the script. Beyond that (more than 3 cameras, more than a few minutes long, other settings, etc.), I have not done. If you need more than this, you should test things out first to make sure the recording gets you what you'd need or expect.

If you run into any problems, please email me at my personal email address, geoff.nagy@gmail.com, since I imagine that my SFU email address will one day soon be rudely yanked out from underneath me.

## END OF README
