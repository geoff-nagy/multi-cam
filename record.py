# -------------------------------------------------------------
# record.py
# a simple script for recording multiple webcam streams to disk
# Geoff Nagy
# -------------------------------------------------------------

import sys as sys		# for arg reading
import subprocess		# for command-line process invocation
import os			# for folder creation
import time			# for tracking recorded time
import signal			# for graceful exiting
import datetime			# for folder date naming

# - - - globals - - - #

# requested video settings; change these as desired, but note that streaming
# using a camera that does not support these settings may fail silently,
# so double-check your cameras' capabilities! (see the nearby README file
# on how to do that)
VIDEO_FORMAT = "jpeg"				# rgb15, rgb24 are other options, but generate larger files
VIDEO_SIZE = "1280x720"				# a good mix between supported webcam resolutions and file size
VIDEO_FRAME_RATE = "30"
VIDEO_MAX_TIME_DD_HH_MM_SS = "99:00:00:00"	# video streams will end automatically after this amount of time
						# [note]: currently, the script itself does not terminate after this amount, though
						# [todo]: so, maybe fix that in the future some time if we really need >99 days

# - - - signal handlers - - - #

# [todo]: actually check signal we get; oh well!
def signalHandler(sig, frame):
	global shouldQuit
	shouldQuit = True

# - - - main processing - - - #

# intro
print
print("-------------------------------------------------------------")
print("record.py")
print("A simple script for recording multiple webcam streams to disk")
print("Geoff Nagy")
print("-------------------------------------------------------------")
print

# ensure we got the correct number of args
if len(sys.argv) < 3:
	print("    usage: " + sys.argv[0] + " <output_dir> <cam_1> ... <cam_N>")
	print("    e.g. : " + sys.argv[0] + " camera_out /dev/video0 /dev/video1")
	print
	exit(1)

# grab folder name and append date and time
folderName = sys.argv[1] + "_" + datetime.datetime.now().strftime("%d_%B_%Y__%H_%M_%S")

# check to make sure all video devices exist
print("-- checking that all specified video devices exist...")
for i in range(2, len(sys.argv)):
	if not os.path.exists(sys.argv[i]):
		print("     [ERR] video device \"" + sys.argv[i] + "\" does not exist")
		exit(1)
	else:
		print("     [OK] video device \"" + sys.argv[i] + "\" exists")

# output number of video streams to capture, and inform the user
numVideos = len(sys.argv) - 2
print("-- will record from " + str(numVideos) + " camera(s), to folder \"" + folderName + "\"")

# confirm yes/no
choice = '0'
while choice != 'y' and choice != 'n':
	print("   start recording (y/n)?")
	choice = raw_input().lower()

# abort?
if choice == 'n':
	print("aborting")
	exit(1)

# create the folder where the videos go
os.makedirs(folderName)

# start the timer and begin recording
print("-- starting streams...")
startTime = time.time()
videoStreams = []
for i in range(2, len(sys.argv)):

	# print out command so user sees what's going on
	command = ["streamer", "-c", sys.argv[i], "-f", VIDEO_FORMAT, "-s", VIDEO_SIZE, "-t", VIDEO_MAX_TIME_DD_HH_MM_SS,  "-r", VIDEO_FRAME_RATE, "-o", folderName + "/cam_" + str(i - 2) + ".avi"]
	print("     " + " ".join(command))
	
	# use this command instead of the one below it; for some bizarre reason, the commented-out line
	# below (where stdout and stderr are hidden) results in only 42-second videos, which is obviously
	# not desirable
	videoStreams.append(subprocess.Popen(command))

	# same thing as above, but no output from streamer is printed
	# [note]: for some reason, using this line results in a video cap of 42 seconds
	#videoStreams.append(subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE))

print("-- to end video recording, use Ctrl-C to terminate")

# set up signal handler
signal.signal(signal.SIGINT, signalHandler)

# loop endlessly until user quits with SIGINT
global shouldQuit
shouldQuit = False
while not shouldQuit:

	# don't use up CPU resources
	time.sleep(0.5)

# complete
elapsedTime = time.time() - startTime
print
print("-- completed in " + time.strftime("%Hh %Mm %Ss", time.gmtime(elapsedTime)))
print

# done
